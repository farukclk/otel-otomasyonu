﻿using System.Diagnostics;
using System.Security.Claims;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using Microsoft.AspNetCore.Authentication.Cookies;



namespace WebApplication1.Controllers;

public class HomeController : Controller
{
    
    DataContext db = new DataContext();
     
   [Authorize]
    public IActionResult Index()
    {

        int bosOda = 0, toplamOda = 0;
        var Odalar = db.odalar.Select(i => new {i.aktif}).ToList();
        foreach (var oda in Odalar )
        {
            if (oda.aktif == false)
                bosOda++;
            toplamOda++;
        }

        ViewBag.bos_oda_sayisi = bosOda ;
        ViewBag.toplam_oda_sayisi = toplamOda;
        return View();
    }

     
    [Authorize]
    public IActionResult Giris()
    {
        List<int> _1kisilikOdalar = new List<int>();
        List<int> _2kisilikOdalar = new List<int>();
        List<int> _3kisilikOdalar = new List<int>();
        List<int> _4kisilikOdalar = new List<int>();

        int bosOdalar = 0;
        foreach (var oda in db.odalar)
        {
            if (oda.aktif == false)
            {
                bosOdalar++;
                if (oda.yatak_sayisi == 1) _1kisilikOdalar.Add(oda.odaNo);
                if (oda.yatak_sayisi == 2) _2kisilikOdalar.Add(oda.odaNo);
                if (oda.yatak_sayisi == 3) _3kisilikOdalar.Add(oda.odaNo);
                if (oda.yatak_sayisi == 4) _4kisilikOdalar.Add(oda.odaNo);
                    
            }
                
        }
        ViewBag._1kisilikOdalar = _1kisilikOdalar;
        ViewBag._2kisilikOdalar = _2kisilikOdalar;
        ViewBag._3kisilikOdalar = _3kisilikOdalar;
        ViewBag._4kisilikOdalar = _4kisilikOdalar;

   
        if (bosOdalar == 0)
        {
            ViewBag.Mesaj = "tum odalar dolu"; 
        }
        
        // select default empty room when first open
  //    else if(HttpContext.Request.Query["yatak"].ToString())
/*
        else if (HttpContext.Request.Query["oda_no"].ToString() != "" &&  odaList.Exists(i =>i.ToString() ==HttpContext.Request.Query["oda_no"].ToString()))
        {
            
            ViewBag.oda_no = HttpContext.Request.Query["oda_no"].ToString();
        }
        else
        {
           
            //ViewBag.oda_no = odaList[0];
        }*/
        return View();
    }
    
    [Authorize, HttpPost]
    public IActionResult Giris(string isim, string soyisim, long tc, string telNo, int odaNo)
    {
        Console.WriteLine("---");
        Console.WriteLine(odaNo);
        
        
        
        
        if (db.musteriler.FirstOrDefault(i => i.Tc == tc) !=null)
        {
            ViewBag.Mesaj = "zaten kayıtli musteri";
        }
        
        else
        {
            
            Oda oda = db.odalar.FirstOrDefault(i => i.odaNo == odaNo);
            
            if (oda != null && oda.aktif == true)
            {
                ViewBag.Mesaj = "sectiginiz oda zaten dolu";
            }
            else
            {
                try
                {
                    oda.aktif = true;
                    telNo = Regex.Replace(telNo, "[^0-9.]", "");
                    db.musteriler.Add(new Musteri() {isim = isim, soyisim = soyisim, Tc = tc, telNo = Convert.ToInt64(telNo), OdaNo = odaNo});
                    db.SaveChanges();

                    ViewBag.Mesaj = "kayit olsuturuldu";
                }
                catch
                {
                    try
                    {
                        oda.aktif = false; // oda yoksa tekrar hata kontrol
                        db.SaveChanges();
                    }
                    catch
                    {
                        ViewBag.Mesaj = "bir hata olustu";
                    }

                    ViewBag.Mesaj = "bir hata olustu";
                }
            }
        }


        List<int> _1kisilikOdalar = new List<int>();
        List<int> _2kisilikOdalar = new List<int>();
        List<int> _3kisilikOdalar = new List<int>();
        List<int> _4kisilikOdalar = new List<int>();

        int bosOdalar = 0;
        foreach (var oda in db.odalar)
        {
            if (oda.aktif == false)
            {
                bosOdalar++;
                if (oda.yatak_sayisi == 1) _1kisilikOdalar.Add(oda.odaNo);
                if (oda.yatak_sayisi == 2) _2kisilikOdalar.Add(oda.odaNo);
                if (oda.yatak_sayisi == 3) _3kisilikOdalar.Add(oda.odaNo);
                if (oda.yatak_sayisi == 4) _4kisilikOdalar.Add(oda.odaNo);
                    
            }
                
        }
        ViewBag._1kisilikOdalar = _1kisilikOdalar;
        ViewBag._2kisilikOdalar = _2kisilikOdalar;
        ViewBag._3kisilikOdalar = _3kisilikOdalar;
        ViewBag._4kisilikOdalar = _4kisilikOdalar;

        //return RedirectToAction("Giris", "Home");
        return View();
    }

    
    
    
    
    [Authorize]
    public IActionResult Cikis()
    {
        return View();
    }

    [Authorize, HttpPost]
    public IActionResult Cikis(long Tc, int url)
    {
        var musteri = db.musteriler.FirstOrDefault(i => i.Tc == Tc);

        if (musteri != null)
        {
            db.musteriler.Remove(musteri);
            Oda oda = db.odalar.FirstOrDefault(i => i.odaNo== musteri.OdaNo);
            oda.aktif = false;
            db.SaveChanges();
            ViewBag.Mesaj = "musteri silindi";

        }
        else
        {
            ViewBag.Mesaj = "müsteri bulunamadi";
        }

        if (url == 1)
            return RedirectToAction("Liste", "Home");
        else
            return View();

    }



    [Authorize]
    public IActionResult Liste()
    {
        List<Musteri> musteriler = db.musteriler.ToList();
        ViewBag.liste = musteriler;
       
        return View(musteriler);
    }

    
    [Authorize]
    public IActionResult OdaListe()
    {
        List<Oda> odalar = db.odalar.ToList();
        ViewBag.odalar = odalar;
        return View();
    }
   
    public IActionResult Login()
    {
        return View();
    }
    
    
    [HttpPost]
    public async Task<IActionResult> Login(Calisan calisan)
    {
        if (calisan.password == "12345" && calisan.username == "admin")
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, "a"),
         
            
            };
            var useridentity = new ClaimsIdentity(claims, "Login");
            ClaimsPrincipal principal = new ClaimsPrincipal(useridentity);
            await HttpContext.SignInAsync(principal);
            return RedirectToAction("Index", "Home");
        }
        else
        {
            ViewBag.Mesaj = "hatali sifre";
            return View();
        }
    }
    
 
    
    [Authorize]
    public async Task<IActionResult> Logout()
    {
        await HttpContext.SignOutAsync(
            CookieAuthenticationDefaults.AuthenticationScheme);

        return RedirectToAction("Index", "Home");
     
    }
    
    [Authorize]
    public IActionResult Privacy()
    {
        return View();
    }

    public IActionResult Test()
    {
        ViewBag.liste = db.musteriler;
        return View();
    }
  
    [HttpPost]
    public IActionResult Test(string isim)
    {
        Console.WriteLine(isim == null);
        return View();
    }
    
    
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }

  
}


