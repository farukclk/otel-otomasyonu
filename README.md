# REQUIREMENTS:
 
dotnet core 6
  
<br>

# how to run:

     git clone https://gitlab.com/farukclk/otel-otomasyonu
     cd otel-otomasyonu-asp.net
     dotnet run

<br><br>

# default user and pass: 

admin : 12345<br>
you can change these from the Controllers/HomeController.cs file.


<br>

# for database settings:

edit Models/DataContext.cs file

<br>

<img src="g1.png"></img>

<img src="g2.png"></img>

<img src="g3.png"></img>
