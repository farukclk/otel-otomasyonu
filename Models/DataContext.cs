/*
 * database connection class
 * include mssql and sqlite
 */

using Microsoft.EntityFrameworkCore;

namespace WebApplication1.Models;


public class DataContext:DbContext
{
 
    public DbSet<Oda> odalar  { set; get;}
    public DbSet<Musteri> musteriler { set; get; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        // for msql
        //optionsBuilder.UseSqlServer(@"WRITE_HERE_YOUR_CONNECTION_STRING");  
        
        
        // for sqlite
        optionsBuilder.UseSqlite("Data Source=database.db");  
    }
  
    
    
    /*

    public  SqlConnection cnn  = new SqlConnection(connetionString);

    public DataContext()
    {
        cnn.Open();
    }

    */

 //public DbSet<Oda> odalar  { set; get;}
 
 //public virtual DbSet<Muster> Rooms { get; set; }
 //public virtual DbSet<Reservation> Reservations { get; set; }

     
 
}  