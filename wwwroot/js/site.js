﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.


if (sessionStorage.darkMode == null) {
    sessionStorage.setItem("darkMode", "1");
}
else {
    if (sessionStorage.darkMode == "1") { // default dark mode acik
   /*     $( "nav" ).removeClass( "bg-white" );
        $( "nav" ).addClass("bg-dark");
        $( "body" ).addClass("bg-dark");
        $("table").addClass("table-dark");
        document.body.style.color="white";*/

    }
    else {
        $( "body" ).removeClass( "bg-dark" );
        $( "nav" ).removeClass( "bg-dark" );
        $("table").removeClass("table-dark");
        $( "nav" ).addClass( "bg-white" );

        document.body.style.color="black";
    
    }
}


function karanlikMod() {
    if(sessionStorage.darkMode == "1" ) {
        $( "body" ).removeClass( "bg-dark" );
        $( "nav" ).removeClass( "bg-dark" );
        $("table").removeClass("table-dark");
        $( "nav" ).addClass( "bg-white" );

        document.body.style.color="black";
        sessionStorage.darkMode = "0";
    }
    else {
        $( "nav" ).removeClass( "bg-white" );
        $( "nav" ).addClass("bg-dark");
        $( "body" ).addClass("bg-dark");
        $("table").addClass("table-dark");
        document.body.style.color="white";
        sessionStorage.darkMode = "1";
    }
}
